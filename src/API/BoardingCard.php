<?php

namespace API;

use API\Transport\Bus;
use API\Transport\Plane;
use API\Transport\Train;

/**
 * Class BoardingCard
 *
 * @package API
 */
class BoardingCard
{
    /**
     * @var $collection array Array of cards
     * @var $transport array Default set of transport
     */
    private $collection = [];
    protected static $transport = [
        'train' => Train::class,
        'bus' => Bus::class,
        'plane' => Plane::class,
    ];

    /**
     * BoardingCard constructor.
     *
     * @param array $collection
     */
    public function __construct(array $collection)
    {
        $this->collection = $collection;
    }

    /**
     * Find start and end of destinations
     *
     * @return BoardingCard
     */
    private function extractStartEndDestinations(): self
    {
        for ($i = 0, $max = count($this->collection); $i < $max; $i++) {
            $hasPreviousCard = false;
            $isLastTrip = true;

            foreach ($this->collection as $card) {
                if (strcasecmp($this->collection[$i]['departure'], $card['arrival']) === 0) {
                    $hasPreviousCard = true;
                } elseif (strcasecmp($this->collection[$i]['arrival'], $card['departure']) === 0) {
                    $isLastTrip = false;
                }
            }

            if (!$hasPreviousCard) {
                array_unshift($this->collection, $this->collection[$i]);
                unset($this->collection[$i]);
            } elseif ($isLastTrip) {
                array_push($this->collection, $this->collection[$i]);
                unset($this->collection[$i]);
            }
        }

        $this->collection = array_values($this->collection);

        return $this;
    }

    /**
     * Sort a cards collection from Departure to Arrival
     *
     * @return BoardingCard
     */
    public function sort(): self
    {
        return $this->extractStartEndDestinations()->paringCards();
    }

    /**
     * Pairing cards
     *
     * @return BoardingCard
     */
    private function paringCards(): self
    {
        for ($i = 0, $max = count($this->collection) - 1; $i < $max; $i++) {
            foreach ($this->collection as $index => $card) {
                if (strcasecmp($this->collection[$i]['arrival'], $card['departure']) == 0) {
                    $nextIndex = $i + 1;

                    $tempRow = $this->collection[$nextIndex];
                    $this->collection[$nextIndex] = $card;
                    $this->collection[$index] = $tempRow;

                    break;
                }
            }
        }

        return $this;
    }

    /**
     * Get sorted transportation as an array of objects
     *
     * @return array
     *
     * @throws Exception\TripSorterException
     */
    public function getTransport(): array
    {
        $transportList = [];

        if (count($this->collection) == 0) {
            return $transportList;
        }

        foreach ($this->collection as $trip) {
            $type = strtolower($trip['transport']);

            if (!isset(static::$transport[$type])) {
                throw new Exception\TripSorterException(
                    sprintf(
                        'Unsupported transport : %s',
                        $type
                    )
                );
            }
            $transportList[] = new static::$transport[$type]($trip);
        }

        return $transportList;
    }

    /**
     * Get the sorted Cards
     *
     * @return array
     */
    public function getCollection(): array
    {
        return $this->collection;
    }
}
