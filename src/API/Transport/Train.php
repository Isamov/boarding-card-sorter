<?php

namespace API\Transport;

/**
 * Class Train
 *
 * @package API\Transport
 */
class Train extends AbstractTransport
{
    /**
     * @var $transportNumber string
     * @var $seat string
     */
    protected $transportNumber;
    protected $seat;

    const MESSAGE = 'Take train %s';
    const MESSAGE_FROM_TO = ' from %s to %s. ';
    const MESSAGE_SEAT = 'Sit in seat %s.';

    /**
     * Return a message for the trip, defined in TransportInterface
     *
     * @return string
     */
    public function getMessage(): string
    {
        $message = sprintf(static::MESSAGE, $this->transportNumber);

        $message = sprintf(
            $message . static::MESSAGE_FROM_TO,
            $this->departure,
            $this->arrival
        );

        $message = sprintf($message . static::MESSAGE_SEAT, $this->seat);
        return $message;
    }
}
