<?php

namespace API\Transport;

/**
 * Interface TransportInterface
 *
 * @package API\Transport
 */
interface TransportInterface
{
    /**
     * Return a transport message
     *
     * @return string
     */
    public function getMessage(): string;
}
