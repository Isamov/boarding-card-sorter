<?php

namespace API\Transport;

/**
 * Class Plane
 *
 * @package API\Transport
 */
class Plane extends AbstractTransport
{
    /**
     * @var $transportNumber string
     * @var $seat string
     * @var $gate string
     * @var $baggage string
     */
    protected $transportNumber;
    protected $seat;
    protected $gate;
    protected $baggage;

    const MESSAGE = 'From %s take flight %s to %s. Gate %s, seat %s.';
    const MESSAGE_BAGGAGE_TICKET = 'Baggage drop at ticket counter %s.';
    const MESSAGE_NO_BAGGAGE_TICKET = 'Baggage will we automatically transferred from your last leg.';

    /**
     * Return a message for the trip, defined in TransportInterface
     *
     * @return string
     */
    public function getMessage(): string
    {
        $message = sprintf(
            static::MESSAGE,
            $this->departure,
            $this->transportNumber,
            $this->arrival,
            $this->gate,
            $this->seat
        );

        $message .= static::MESSAGE_NO_BAGGAGE_TICKET;

        if (!empty($this->baggage)) {
            $message = sprintf($message . static::MESSAGE_BAGGAGE_TICKET, $this->baggage);
        }

        return $message;
    }
}
