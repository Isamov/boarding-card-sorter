<?php

namespace API\Transport;

/**
 * Class AbstractTransport
 *
 * @package API\Transport
 */
abstract class AbstractTransport implements TransportInterface
{
    /**
     * @var $departure string
     * @var $arrival string
     */
    protected $departure;
    protected $arrival;

    const FINAL_DESTINATION_MESSAGE = 'You have arrived at your final destination.';

    /**
     * @param array $trip
     */
    public function __construct(array $trip)
    {
        foreach ($trip as $key => $value) {
            // Replace underscore(_) by CamelCase to match the attribute
            $property = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $key))));

            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }
}
