<?php

namespace API\Exception;

/**
 * Class TripSorterException
 *
 * @package API\Exception
 */
class TripSorterException extends \Exception
{
}
