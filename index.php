<?php

use API\BoardingCard;
use API\Transport\AbstractTransport;

require __DIR__ . '/vendor/autoload.php';

$cards = json_decode(file_get_contents('cards.json'), JSON_OBJECT_AS_ARRAY);

$boardingCard = new BoardingCard($cards);

$transports = $boardingCard->sort()->getTransport();

header('Content-type: text/plain');

if (count($transports) > 0) {
    foreach ($transports as $index => $type) {
        echo $type->getMessage() . PHP_EOL;
    }

    echo AbstractTransport::FINAL_DESTINATION_MESSAGE . PHP_EOL;
}
