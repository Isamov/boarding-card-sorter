### Installation
Using Composer :

```
composer install
```

Composer you can get from [Composer](https://getcomposer.org/)

### Run the application with built in php web server

```
php -S 0.0.0.0:8080
```

### Run the application with Docker

```
docker-compose up
```

[docker-compose](https://docs.docker.com/compose/reference/up/) Documentation 

### Checking code

```
composer check
```
[PSR-2](http://www.php-fig.org/psr/psr-2/) coding style and Cyclomatic Complexity reporting

### Tests

```
composer test
```