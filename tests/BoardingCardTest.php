<?php

namespace Tests;

use API\BoardingCard;
use PHPUnit\Framework\TestCase;

class BoardingCardTest extends TestCase
{
    private $boardingCard;
    protected $cardCollection = [
        [
            'departure' => 'Moscow',
            'arrival' => 'Saint Petersburg',
            'transport' => 'Bus',
        ],
        [
            'departure' => 'Rostov',
            'arrival' => 'Minsk',
            'transport' => 'Train',
        ],
        [
            'departure' => 'Minsk',
            'arrival' => 'Moscow',
            'transport' => 'Plane',
            'transport_number' => '11A',
            'seat' => '10A',
            'gate' => '10A',
        ],
    ];

    protected $expectedCardCollection = [
        [
            'departure' => 'Rostov',
            'arrival' => 'Minsk',
            'transport' => 'Train',
        ],
        [
            'departure' => 'Minsk',
            'arrival' => 'Moscow',
            'transport' => 'Plane',
            'transport_number' => '11A',
            'seat' => '10A',
            'gate' => '10A',
        ],
        [
            'departure' => 'Moscow',
            'arrival' => 'Saint Petersburg',
            'transport' => 'Bus',
        ],
    ];

    public function setUp(): void
    {
        $this->boardingCard = new BoardingCard($this->cardCollection);
    }

    public function testSort(): void
    {
        $sortedCollection = $this->boardingCard->sort()->getCollection();
        $this->assertTrue($this->areSimilar($this->expectedCardCollection, $sortedCollection));
    }

    public function testGetTransport(): void
    {
        $transportation = $this->boardingCard->getTransport();

        foreach ($transportation as $type) {
            $this->assertInstanceOf(\API\Transport\AbstractTransport::class, $type);
        }
    }

    private function areSimilar($expectedCollection, $sortedCollection): bool
    {
        foreach ($sortedCollection as $key => $collection) {
            if ($expectedCollection[$key]['departure'] != $collection['departure']) {
                return false;
            }
        }

        return true;
    }
}
